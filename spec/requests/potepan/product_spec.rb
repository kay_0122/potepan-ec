require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:taxon) { create(:taxon, name: "taxon", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:another_taxon) { create(:taxon, name: "another_taxon", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:another_product) { create(:product, taxons: [another_taxon]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

  before do
    related_products.each { |related_product| related_product.images << create(:image) }
  end

  describe "GET /show" do
    before do
      get potepan_product_path(product.id)
    end

    context "ページ全体に関するテスト" do
      it "正常なレスポンスが返ってくること" do
        expect(response).to have_http_status(200)
      end

      it "名前が含まれているか" do
        expect(response.body).to include product.name
      end

      it "価格が含まれているか" do
        expect(response.body).to include product.price.to_s
      end

      it "説明が含まれているか" do
        expect(response.body).to include product.description
      end
    end

    context "関連商品に関するテスト" do
      it "関連した商品のデータが取得できていること" do
        expect(assigns(:related_products)).to match_array related_products
        expect(assigns(:related_products)).not_to include product
        expect(assigns(:related_products)).not_to include another_product
      end
    end
  end
end
