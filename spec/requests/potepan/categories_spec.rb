RSpec.describe "Potepan::Categories", type: :request do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon) { create(:taxon, name: "bag", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, name: "rails_bag", price: 200, taxons: [taxon]) }

  describe "GET potepan/categories/:id" do
    before do
      get potepan_category_path(taxon.id)
    end

    it "正常なレスポンスが返ってくること" do
      expect(response).to have_http_status(200)
    end

    it "taxonの名前" do
      expect(response.body).to include "bag"
    end

    it "taxonomyの名前" do
      expect(response.body).to include "Categories"
    end

    it "productの名前" do
      expect(response.body).to include "rails_bag"
    end

    it "productの価格" do
      expect(response.body).to include "200"
    end
  end
end
