require 'rails_helper'

RSpec.describe Spree::ProductDecorator, type: :model do
  let(:taxon) { create(:taxon) }
  let(:another_taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:related_product) { create(:product, taxons: [taxon]) }
  let(:related_product2) { create(:product, taxons: [taxon]) }
  let(:another_product) { create(:product, taxons: [another_taxon]) }

  describe "product_decoratorのテスト" do
    it "関連商品の取得できている" do
      expect(product.related_products).to include(related_product, related_product2)
    end
    it "関連しない商品は取得していない" do
      expect(product.related_products).not_to include(another_product)
    end
    it "詳細ページの商品を取得してない" do
      expect(product.related_products).not_to include(product)
    end
    it "同じ商品を取得しない" do
      expect(product.related_products).not_to match_array([product, product])
    end
  end
end
