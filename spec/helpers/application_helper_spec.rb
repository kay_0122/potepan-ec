require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'title' do
    it "動的に表示する" do
      expect(helper.full_title("test")).to eq "test - BIGBAG Store"
      expect(helper.full_title("")).to eq "BIGBAG Store"
      expect(helper.full_title(nil)).to eq "BIGBAG Store"
    end
  end
end
