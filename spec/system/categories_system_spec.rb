require 'rails_helper'

RSpec.describe 'Category', type: :system do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon) { create(:taxon, name: "bag", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, name: "rails-bag", price: 200, taxons: [taxon]) }
  let!(:another_taxon) { create(:taxon, name: "tops", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:another_product) { create(:product, name: "rails-tops", price: 300, taxons: [another_taxon]) }

  describe "画面表示のテスト" do
    it 'カテゴリー画面が表示される' do
      visit potepan_category_path(taxon.id)
      expect(page).to have_content(taxonomy.name)
      expect(page).to have_content(taxon.name)
    end
  end

  describe "サイドバーのテスト" do
    before do
      visit potepan_category_path(taxon.id)
    end

    it "サイドバーのtaxonomy,taxon表示が正しいか" do
      within ".side-nav" do
        expect(page).to have_content "Categories"
        expect(page).to have_content "bag"
        expect(page).to have_content "tops"
      end
    end

    it "選択した商品カテゴリーが正しい商品を表示されるか" do
      within ".side-nav" do
        click_link "bag"
      end
      expect(current_path).to eq potepan_category_path(taxon.id)
      expect(page).to have_selector ".page-title", text: "bag"
      expect(page).to have_selector ".productCaption", text: "rails-bag"
      expect(page).to have_selector ".productCaption", text: "200"
    end
    it "選択した商品カテゴリー以外の商品を表示しない" do
      within ".side-nav" do
        click_link "bag"
      end
      expect(current_path).not_to eq potepan_category_path(another_taxon.id)
      expect(page).to have_no_selector ".page-title", text: "tops"
      expect(page).to have_no_selector ".productCaption", text: "rails-tops"
      expect(page).to have_no_selector ".productCaption", text: "300"
    end
  end
end
