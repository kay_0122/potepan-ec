require 'rails_helper'

RSpec.describe 'Product', type: :system do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon) { create(:taxon, name: "bag", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, name: "rails_bag", price: 200, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

  before do
    product.images << create(:image)
    related_products.each { |related_product| related_product.images << create(:image) }
  end

  describe "一覧ページへ戻るリンクのテスト" do
    context "productにtaxonが設定されている場合" do
      it "リンクが表示されていること" do
        visit potepan_product_path(product.id)
        click_link "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(taxon.id)
        expect(page).to have_selector ".page-title", text: "bag"
      end
    end

    context "productにtaxonがnilの場合" do
      let(:another_product) { create(:product) }

      it "リンクが表示されないこと" do
        visit potepan_product_path(another_product.id)
        expect(current_path).to eq potepan_product_path(another_product.id)
        expect(page).not_to have_link "一覧ページへ戻る"
      end
    end
  end

  describe "関連商品に関するテスト" do
    let(:another_product) { create(:product) }

    before do
      visit potepan_product_path(product.id)
    end

    it "関連商品が表示される" do
      expect(page).to have_selector ".productCaption h5", text: related_products.first.name
      expect(page).to have_selector ".productCaption h3", text: related_products.first.display_price
    end

    it "関連していない商品は表示しない" do
      expect(page).not_to have_selector "productCaption h5", text: another_product.name
      expect(page).not_to have_selector "productCaption h3", text: another_product.display_price
    end

    it "関連商品が４つ表示される" do
      within(".productsContent") do
        expect(page).to have_selector(".productBox", count: 4)
      end
    end

    it "関連商品の詳細ページに遷移する" do
      click_link related_products.first.name
      expect(current_path).to eq potepan_product_path(related_products.first.id)
      expect(page).to have_content(related_products.first.name)
      expect(page).to have_content(related_products.first.display_price)
      expect(page).to have_content(related_products.first.description)
    end
  end
end
